//
//  ApiManager.swift
//  translate.ai
//
//  Created by Alexis Levasseur on 18/07/2023.
//

import Foundation

struct TranslationResponse: Codable {
    struct Translation: Codable {
        let detectedSourceLanguage: String
        let text: String
        
        enum CodingKeys: String, CodingKey {
            case detectedSourceLanguage = "detected_source_language"
            case text
        }
    }
    
    let translations: [Translation]
    
    enum CodingKeys: String, CodingKey {
        case translations
    }
}

class APIManager {
    func translateText(text: String, targetLang: String) async throws -> TranslationResponse {
        guard let authKey = ProcessInfo.processInfo.environment["DEEPL_AUTH_KEY"] else {
            throw NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Missing DEEPL_AUTH_KEY in environment variables."])
        }
        
        let url = URL(string: "https://api-free.deepl.com/v2/translate")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("DeepL-Auth-Key \(authKey)", forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let bodyData = "text=\(text)&target_lang=\(targetLang)".data(using: .utf8)
        request.httpBody = bodyData
        
        let (data, _) = try await URLSession.shared.data(for: request)
        return try JSONDecoder().decode(TranslationResponse.self, from: data)
    }
}
