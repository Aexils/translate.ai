//
//  translate_aiApp.swift
//  translate.ai
//
//  Created by Alexis Levasseur on 18/07/2023.
//

import SwiftUI

@main
struct translate_aiApp: App {
    var body: some Scene {
        WindowGroup {
            TranslationView()
        }
    }
}
