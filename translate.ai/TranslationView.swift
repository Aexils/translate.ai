//
//  ContentView.swift
//  translate.ai
//
//  Created by Alexis Levasseur on 18/07/2023.
//

import SwiftUI
import SwiftUIX
import AppKit

struct TranslationView: View {
    
    @State private var textToTranslate: String = ""
    @State private var translatedText = ""
    @State private var targetLang: String = "EN"
    @State private var isLoad: Bool = false
    @State private var isShowingPopover = false
    @State private var selectedLanguageIndex = 0
        
    let languages: [(language: String, code: String)] = [
        ("English 🇬🇧", "EN"),
        ("French 🇫🇷", "FR"),
        ("German 🇩🇪", "DE"),
        ("Spanish 🇪🇸", "ES"),
        ("Italian 🇮🇹", "IT"),
        ("Portuguese 🇵🇹", "PT"),
        ("Dutch 🇳🇱", "NL"),
        ("Danish 🇩🇰", "DA"),
        ("Swedish 🇸🇪", "SV"),
        ("Norwegian 🇳🇴", "NB"),
        ("Finnish 🇫🇮", "FI"),
        ("Bulgarian 🇧🇬", "BG"),
        ("Czech 🇨🇿", "CS"),
        ("Greek 🇬🇷", "EL"),
        ("Estonian 🇪🇪", "ET"),
        ("Hungarian 🇭🇺", "HU"),
        ("Indonesian 🇮🇩", "ID"),
        ("Japanese 🇯🇵", "JA"),
        ("Korean 🇰🇷", "KO"),
        ("Lithuanian 🇱🇹", "LT"),
        ("Latvian 🇱🇻", "LV"),
        ("Polish 🇵🇱", "PL"),
        ("Romanian 🇷🇴", "RO"),
        ("Russian 🇷🇺", "RU"),
        ("Slovak 🇸🇰", "SK"),
        ("Slovenian 🇸🇮", "SL"),
        ("Turkish 🇹🇷", "TR"),
        ("Ukrainian 🇺🇦", "UK"),
        ("Chinese 🇨🇳", "ZH")
    ]
    
    var body: some View {
        VStack {
            HStack {
                Text("Translate.AI")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding([.top, .leading], 10)
                
                Spacer()
                
                Image("chatgpt-icon-logo")
                    .resizable()
                    .frame(width: 100, height: 30)
                    .padding([.top, .trailing], 10)
            }
            Spacer()
            HStack {
                Picker(selection: $selectedLanguageIndex, label: Text("Select Language")) {
                    ForEach(languages.indices, id: \.self) { index in
                        Text(languages[index].language)
                            .tag(index)
                    }
                }
                .pickerStyle(.menu)
                .padding([.leading, .trailing], 12)
                .onChange(of: selectedLanguageIndex) { newIndex in
                    self.targetLang = languages[newIndex].code
                }
            }
            Spacer()
            HStack {
                Text(translatedText)
                    .font(Font.system(size: 18))
                if (translatedText != "") {
                    Button(action: {
                        NSPasteboard.general.clearContents()
                        NSPasteboard.general.setString(translatedText, forType: .string)
                        self.isShowingPopover = true
                    }, label: {
                        Image(systemName: "clipboard")
                            .font(.system(size: 16))
                    })
                    .buttonStyle(.plain)
                    .popover(isPresented: $isShowingPopover, arrowEdge: Edge.bottom) {
                        Text("copied")
                            .padding()
                    }
                    .onAppear {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            self.isShowingPopover = false
                        }
                    }
                }
            }
            Spacer()
            HStack {
                TextField("Enter text", text: $textToTranslate)
                    .textFieldStyle(.plain)
                    .font(Font.system(size: 18))
                    .padding(10)
                
                if (isLoad) {
                    ActivityIndicator()
                        .animated(true)
                        .style(.large)
                        .padding([.trailing])
                } else {
                    Button(action: {
                        isLoad = true
                        Task {
                            do {
                                let response = try await APIManager().translateText(text: textToTranslate, targetLang: targetLang)
                                if let translation = response.translations.first {
                                    translatedText = translation.text
                                    textToTranslate = ""
                                }
                                isLoad = false
                            } catch {
                                print("Error: \(error.localizedDescription)")
                                isLoad = false
                            }
                        }
                    }, label: {
                        Image(systemName: "paperplane")
                            .font(.system(size: 20))
                            .padding([.trailing])
                    })
                    .buttonStyle(.plain)
                    .keyboardShortcut(.defaultAction)
                }
                
            }
            .overlay(
                Rectangle()
                    .frame(height: 1)
                    .foregroundColor(.black)
                    .padding(.top, -20)
            )
        }
    }
}
